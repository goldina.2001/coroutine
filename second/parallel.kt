import kotlinx.coroutines.*

suspend fun FirstFun ():Int {
    delay(1000L)
    return 10
}

suspend fun SecondFun ():Int {
    delay(1000L)
    return 4
}

fun main() {
    val start = System.currentTimeMillis()
    val num:Int
    runBlocking {
        val firstNum = async{ FirstFun()}
        val secondNum = async{ SecondFun()}
        num = firstNum.await()+secondNum.await()
    }
    val end = System.currentTimeMillis()
    print("Sum = ${num}, TimeWork = ${end - start}")
}

