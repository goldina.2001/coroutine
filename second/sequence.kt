import kotlinx.coroutines.*

suspend fun FirstFun ():Int {
    delay(1000L)
    return 10
}

suspend fun SecondFun ():Int {
    delay(1000L)
    return 4
}

fun main() {
    runBlocking {
        val start = System.currentTimeMillis()
        val num = FirstFun() + SecondFun()
        val end = System.currentTimeMillis()
        print("Sum = ${num}, TimeWork = ${end - start}")
    }
}

