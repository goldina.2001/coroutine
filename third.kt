import kotlinx.coroutines.*

fun main(args: Array<String>) {
    runBlocking<Unit> {
        val job = launch {
            try {
                repeat(3) { i -> println("I'm sleeping $i ...")
                    delay(1000L)
                }

            } finally {
                println("I'm running finally")
            }
        }
        delay(2500L)
        println("main: I'm tired of waiting!")
    }
    println("main: Now I can quit")
}

